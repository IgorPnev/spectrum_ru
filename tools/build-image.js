import { src, dest, watch, parallel, series, lastRun } from 'gulp'

export const buildImage = () => src(['./src/img/**/*.{webp,png,jpg,jpeg,svg}', '!./src/img/inspections/**/*.{webp,png,jpg,jpeg,svg}'])
    .pipe(dest('./build/img'))
