import { src, dest, watch, parallel, series, lastRun } from "gulp"
import plumber from "gulp-plumber"
import svgmin from "gulp-svgmin"
import svgstore from "gulp-svgstore"
import rename from "gulp-rename"
import rev from "gulp-rev"
import revDel from "rev-del"

export const buildSvgBitrix = () =>
  src("./src/svg/**/*.svg")
    .pipe(plumber())
    .pipe(svgmin({
        plugins: [{
            removeViewBox: false
        }]
    }))
    .pipe(
      svgstore({
        inlineSvg: true
      })
    )
    .pipe(rename("sprite.svg"))
    .pipe(rev())
    .pipe(dest("../local/templates/kdsi/svg/"))
    .pipe(dest("./build/svg/"))
    .pipe(rev.manifest())
    .pipe(revDel({ dest: "../local/templates/kdsi/svg/", force: true }))
    .pipe(revDel({ dest: "./build/svg/", force: true }))
    .pipe(dest("../local/templates/kdsi/svg/"))
    .pipe(dest("./build/svg/"))

// export const buildSvg = () =>
//   src(["./src/svg/**/*.svg", "!./src/svg/source/**/*.svg"])
//     .pipe(plumber())
//     .pipe(svgmin({
//         plugins: [{
//             removeViewBox: false
//         }]
//     }))
//     .pipe(
//       svgstore({
//         inlineSvg: true
//       })
//     )
//     .pipe(rename("sprite.svg"))
//     .pipe(dest("./build/img/"))
//     .pipe(dest("../local/templates/kdsi/"))
