import { dest, lastRun, parallel, series, src, watch } from "gulp"

import filter from "gulp-filter"
import fs from "fs"
import gulpif from "gulp-if"
import plumber from "gulp-plumber"
import pug from "gulp-pug"
import pugInheritance from "yellfy-pug-inheritance"
import realFavicon from "gulp-real-favicon"
import util from "gulp-util"

const production = !!util.env.production

let pugInheritanceCache = {}
const pugDirectory = "src/template"
const FAVICON_DATA_FILE = "faviconData.json"

export const buildPug = () => {
  return new Promise((resolve, reject) => {
    const changedFile = global.changedTemplateFile
    const options = {
      changedFile,
      treeCache: pugInheritanceCache
    }

    // Update cache for all files or only for specified file
    pugInheritance.updateTree(pugDirectory, options).then(inheritance => {
      // Save cache for secondary compilations
      pugInheritanceCache = inheritance.tree
      return (
        src([`${pugDirectory}/*.pug`, `!${pugDirectory}/_*.pug`])
          // We can use Cache only for Watch mode
          .pipe(plumber())
          .pipe(gulpif(global.watch, filter(file => pugFilter(file, inheritance))))
          .pipe(
            pug({
              pretty: production ? false : true
            })
          )
          .pipe(
            production
              ? realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code)
              : util.noop()
          )
          .pipe(dest("build"))
          .on("end", resolve)
          .on("error", reject)
      )
    })
  })
}

function pugFilter(file, inheritance) {
  const filepath = `${pugDirectory}/${file.relative}`
  if (inheritance.checkDependency(filepath, global.changedTemplateFile)) {
    console.log(`Compiling: ${filepath}`)
    return true
  }
  return false
}
