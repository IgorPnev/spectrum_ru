import { mapInit } from "./component/map"
import { scrollInit } from "./component/global/scroll"
import { casesInit } from "./component/cases/"
import { sliderInit } from "./component/slider"
import ScrollOut from "scroll-out"
import { popupInit } from "./component/popup"
import { menuInit } from "./component/menu"
import { showCookie } from "./component/cookie"
import { contentInit } from "./component/global/content"
import { projectInit } from "./component/project"
import { pageInit } from "./component/page"
import { contactInit } from "./component/contact"
import { socialWidget } from "./component/social/index"
import LazyLoad from "vanilla-lazyload"

document.addEventListener("DOMContentLoaded", () => {
  const myLazyLoad = new LazyLoad()
  sliderInit()
  projectInit()
  pageInit()
  contactInit()
  // casesInit()
  // scrollInit()
  // mapInit()
  // popupInit()
  menuInit()
  socialWidget()
  // contentInit()
})

window.onload = function() {
  ScrollOut({
    once: true
  })
  showCookie()
};
