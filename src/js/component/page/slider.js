import Glide, {
  Controls,
  Swipe,
  Autoplay
} from "@glidejs/glide/dist/glide.modular.esm"
import { q, qq } from "../../helper/q"

export const sliderInit = () => {
  if (!q(".pageSlider")) return null

  const pageSlider = new Glide(".pageSlider", {
    type: "slider",
    startAt: 0,
    perView: 1,
    gap: 0
  }).mount({
    Controls
    // Swipe
  })
  setTimeout(() => {
    window.dispatchEvent(new Event("resize"))
  }, 100)
};
