import pose from "popmotion-pose/dist/popmotion-pose.es"
import { q, qq } from "helper/q"

const socialProps = {
  initialPose: "close",
  open: {
    delayChildren: 100,
    staggerChildren: 75,
    transition: { duration: 400 }
  },
  close: {
    delayChildren: 100,
    staggerChildren: 50
  }
}

const socialItemProps = {
  initialPose: "close",
  open: {
    opacity: 1,
    x: 0,
    transition: { duration: 300 },
    applyAtStart: { pointerEvents: "all" }
  },
  close: {
    opacity: 0,
    x: 30,
    transition: { duration: 200 },
    applyAtEnd: { pointerEvents: "none" }
  }
}

export const socialWidget = () => {
  const widget = q(".pageFooterSocial")
  if (!widget) return null
  const socialPosed = pose(widget, socialProps)
  const socialItems = qq(".pageFooterItemSocialItem", widget)
  socialItems.map(item => {
    socialPosed.addChild(item, socialItemProps)
  })
  const trigger = q(".pageFooterItem", widget)
  trigger.addEventListener("click", () => {
    if (trigger.dataset.status === "open") {
      trigger.dataset.status = "close"
      socialPosed.set("close")
      return null
    }
    trigger.dataset.status = "open"
    socialPosed.set("open")
  })
}
