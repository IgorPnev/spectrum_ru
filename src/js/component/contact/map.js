/*global google contactMap*/
import { q, qq } from "../../helper/q";
import { stylesMap } from "../map";
export const mapInit = () => {
  const mapElement = q("#addressMap");

  if (mapElement) {
    const mapStores = new google.maps.Map(mapElement, {
      scrollwheel: false,
      zoom: 17,
      minZoom: 2,
      center: new google.maps.LatLng(55.755825, 37.617298),
      streetViewControl: false,
      mapTypeControl: false,
      styles: stylesMap,
      scaleControl: false,
      zoomControl: true,
      fullscreenControl: false
    });

    const bounds = new google.maps.LatLngBounds();
    const markers = [];
    if (typeof contactMap !== "undefined") {
      contactMap.map((item, index) => {
        markers[index] = new google.maps.Marker({
          icon: {
            url: "/img/LOGO_SIGN.svg",
            size: new google.maps.Size(56, 66),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(28, 66),
            scaledSize: new google.maps.Size(56, 66),
            labelOrigin: new google.maps.Point(56, 66)
          },
          position: new google.maps.LatLng(item.latitude, item.longitude),
          map: mapStores,
          optimized: false

          // label: { fontFamily: "lato", text: item.companyInfo.name, color: "white" }
        });

        bounds.extend(new google.maps.LatLng(item.latitude, item.longitude));

        markers[index].addListener("click", () => {
          mapStores.setZoom(16);
          mapStores.setCenter(markers[index].getPosition());
        });
      });
    }

    mapStores.setCenter(
      markers[0]
        ? markers[0].getPosition()
        : new google.maps.LatLng(55.755825, 37.617298)
    );
    mapStores.setZoom(16);
    // mapStores.fitBounds(bounds)

    // window.addEventListener('resize', () => {
    //   mapStores.setCenter(bounds.getCenter())
    //   mapStores.fitBounds(bounds)
    //   mapStores.setZoom(mapStores.getZoom() - 1)
    // })

    const buttonsToZoom = qq(".contactItemHeader");
    buttonsToZoom.map((button, index) => {
      button.addEventListener("click", () => {
        setTimeout(() => {
          mapStores.setZoom(16);
          mapStores.setCenter(markers[index].getPosition());
        }, 500);
      });
    });
  }
};
