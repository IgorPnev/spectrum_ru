import Glide, {
  Controls,
  Swipe,
  Autoplay,
  Breakpoints,
} from '@glidejs/glide/dist/glide.modular.esm'
import { q } from '../../helper/q'
import { mapInit } from './map'

export const projectInit = () => {
  mapInit()
  if (!q('.projectSlider')) return
  const slider = new Glide('.projectSlider', {
    type: 'slider',
    startAt: 0,
    perView: 3,
    gap: 70,
    bound: true,
    breakpoints: {
      1024: {
        perView: 2,
      },
      768: {
        perView: 1,
      },
    },
  })

  slider.mount({
    Controls,
    Swipe,
    Autoplay,
    Breakpoints,
  })
  setTimeout(() => {
    window.dispatchEvent(new Event('resize'))
  }, 200)
}
