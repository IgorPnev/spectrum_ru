/*global google contactMap*/
import { q, qq } from "../../helper/q"
import { stylesMap } from "../map"
// import { mapMarkers } from "./var"

export const mapInit = () => {
  const mapElement = q("#projectMap")

  if (mapElement) {
    const mapStores = new google.maps.Map(mapElement, {
      scrollwheel: false,
      zoom: 17,
      minZoom: 2,
      center: new google.maps.LatLng(55.755825, 37.617298),
      streetViewControl: false,
      mapTypeControl: false,
      styles: stylesMap,
      scaleControl: false,
      zoomControl: true,
      fullscreenControl: false
    })

    const bounds = new google.maps.LatLngBounds()
    const markers = {}

    mapMarkers.map((item, index) => {
      markers[index] = new google.maps.Marker({
        icon: {
          url: "/img/LOGO_SIGN.svg",
          size: new google.maps.Size(56, 66),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(28, 66),
          scaledSize: new google.maps.Size(56, 66),
          labelOrigin: new google.maps.Point(56, 66)
        },
        position: new google.maps.LatLng(item.latitude, item.longitude),
        map: mapStores,
        optimized: false

        // label: { fontFamily: "lato", text: item.companyInfo.name, color: "white" }
      })

      bounds.extend(new google.maps.LatLng(item.latitude, item.longitude))
      const infowindow = new google.maps.InfoWindow()

      markers[index].addListener("click", () => {
        const currentZoom = mapStores.getZoom()
        infowindow.setContent(
          getInfoWindowContent(mapMarkers[index].name, mapMarkers[index].image)
        )
        infowindow.open(mapStores, markers[item.id])
        infowindow.addListener("domready", () => {
          q(".infoWindowClose").addEventListener("click", () => {
            infowindow.close()
            mapStores.setZoom(currentZoom)
            mapStores.fitBounds(bounds)
          })
        })
        mapStores.setZoom(15)
        mapStores.setCenter(markers[index].getPosition())
      })
    })

    mapStores.setCenter(markers[0].getPosition())
    mapStores.setZoom(16)
    mapStores.fitBounds(bounds)

    window.addEventListener("resize", () => {
      mapStores.setCenter(bounds.getCenter())
      mapStores.fitBounds(bounds)
      mapStores.setZoom(mapStores.getZoom() - 1)
    })

    const buttonsToZoom = qq(".contactItemHeader")
    buttonsToZoom.map((button, index) => {
      button.addEventListener("click", () => {
        setTimeout(() => {
          mapStores.setZoom(14)
          mapStores.setCenter(markers[index].getPosition())
        }, 500)
      })
    })
  }
}

const getInfoWindowContent = (name, image) => {
  return `
  <div class="mapInfoWindow">
  <div class="infoWindowImage"><img src="${image}" alt="" /></div>
  <div class="infoWindowTitle">${name}</div>
  <div class="infoWindowClose">
    <svg class="icon-krest"><use xlink:href="#icon-krest" /></svg>
  </div>
</div>
  `
}
