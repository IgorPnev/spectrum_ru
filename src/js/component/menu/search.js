import { q, qq } from "helper/q"
import pose from "popmotion-pose"

const searchProps = {
  initialPose: "close",
  open: {
    delayChildren: 100,
    staggerChildren: 75,
    transition: { duration: 400 }
  },
  close: {
    delayChildren: 100,
    staggerChildren: 75
  }
}

const menuProps = {
  initialPose: "close",
  open: {
    opacity: 0,
    y: -30,
    transition: { duration: 300 }
  },
  close: {
    opacity: 1,
    y: 0,
    transition: { duration: 300 }
  }
}

const searchInputProps = {
  initialPose: "close",
  open: {
    opacity: 1,
    y: -18,
    transition: { duration: 300 },
    applyAtStart: { pointerEvents: "all" }
  },
  close: {
    opacity: 0,
    y: 12,
    transition: { duration: 300 },
    applyAtEnd: { pointerEvents: "none" }
  }
}
const searchButtonProps = {
  initialPose: "close",
  open: {
    opacity: 0,
    y: -25,
    transition: { duration: 300 }
  },
  close: {
    opacity: 1,
    y: 0,
    transition: { duration: 300 }
  }
}

const langProps = {
  initialPose: "close",
  open: {
    opacity: 0,
    y: -30,
    transition: { duration: 300 }
  },
  close: {
    opacity: 1,
    y: 0,
    transition: { duration: 300 }
  }
}

const searchCloseButtonProps = {
  initialPose: "close",
  open: {
    opacity: 1,
    y: -20,
    transition: { duration: 300 },
    applyAtStart: { pointerEvents: "all" }
  },
  close: {
    opacity: 0,
    y: 0,
    transition: { duration: 200 },
    applyAtEnd: { pointerEvents: "none" }
  }
}

export const searchInit = () => {
  const trigger = q(".headerSearchButton")
  const searchPosed = pose(q(".headerSearchButton"), searchProps)
  searchPosed.addChild(q(".headerMenu"), menuProps)
  searchPosed.addChild(q(".buttonSearch", trigger), searchButtonProps)
  searchPosed.addChild(q(".buttonSearchClose", trigger), searchCloseButtonProps)
  searchPosed.addChild(q(".headerSearch"), searchInputProps)

  trigger.addEventListener("click", () => {
    if (trigger.dataset.status === "open") {
      trigger.dataset.status = "close"
      searchPosed.set("close")

      return null
    }
    trigger.dataset.status = "open"
    q(".headerSearch input").focus()
    searchPosed.set("open")
  })
}

export const searchMobileInit = () => {
  const trigger = q(".mobileSearchButton")
  const searchPosed = pose(trigger, searchProps)
  searchPosed.addChild(q(".mobileLang"), langProps)
  searchPosed.addChild(q(".buttonSearch", trigger), searchButtonProps)
  searchPosed.addChild(q(".buttonSearchClose", trigger), searchCloseButtonProps)

  searchPosed.addChild(q(".mobileSearch"), searchInputProps)

  trigger.addEventListener("click", () => {
    if (trigger.dataset.status === "open") {
      trigger.dataset.status = "close"
      searchPosed.set("close")
      return null
    }
    trigger.dataset.status = "open"
    q(".mobileSearch input").focus()
    searchPosed.set("open")
  })
}
