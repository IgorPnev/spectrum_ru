import { desktopMenuInit } from "./desktop"
import { searchInit, searchMobileInit } from "./search"
import { mobileMenuInit } from "./mobile"

export const menuInit = () => {
  desktopMenuInit()
  mobileMenuInit()
  searchInit()
  setTimeout(() => {
    searchMobileInit()
  }, 1000)
}
