import { q, qq } from "helper/q"
import pose from "popmotion-pose"

const menuProps = {
  initialPose: "close",
  open: {
    x: 0,
    delayChildren: 400,
    staggerChildren: 150,
    transition: { duration: 600 }
  },
  close: {
    x: "100vw",
    delayChildren: 200,
    staggerChildren: 50
  }
}

const menuItemProps = {
  initialPose: "close",
  open: {
    opacity: 1,
    y: 0,
    transition: { duration: 600 }
  },
  close: {
    opacity: 0,
    y: 60,
    transition: { duration: 200 }
  }
}

const linkProps = {
  initialPose: "close",
  open: {
    opacity: 1,
    y: 0,
    transition: { duration: 600 }
  },
  close: {
    opacity: 0,
    y: 60,
    transition: { duration: 200 }
  }
}

export const mobileMenuInit = () => {
  const state = {
    open: false
  }

  const menu = q(".mobileMenu")
  const button = q(".hamburger")
  if (!menu) return null
  const menuPosed = pose(menu, menuProps)
  const menuItems = qq(".mobileMenuItem")
  const footer = q(".mobileMenuFooter")

  menuItems.map(item => {
    menuPosed.addChild(item, menuItemProps)
    if (item.tagName === "DIV") {
      const next = q(".mobileMenuNext", item.parentNode)
      item.addEventListener("click", () => {
        next.classList.add("mobileMenuNextActive")
      })
      q(".mobileMenuNextBack", next).addEventListener("click", () => {
        next.classList.remove("mobileMenuNextActive")
      })
    } else {
      item.addEventListener("click", () => {
        menuPosed.set("close")
        button.dataset.open = "false"
        button.classList.remove("is-active")
      })
    }
  })

  menuPosed.addChild(footer, linkProps)

  button.addEventListener("click", e => {
    if (button.dataset.open === "true") {
      menuPosed.set("close")
      button.dataset.open = "false"
      button.classList.remove("is-active")
      return null
    }
    button.classList.add("is-active")
    menuPosed.set("open")
    button.dataset.open = "true"
  })
}
