import { q, qq } from "helper/q"
import pose from "popmotion-pose"

const menuProps = {
  initialPose: "close",
  open: {
    opacity: 1,
    delayChildren: 100,
    staggerChildren: 75,
    transition: { duration: 400 },
    applyAtStart: { pointerEvents: "all" }
  },
  close: {
    opacity: 0,
    delayChildren: 100,
    staggerChildren: 50,
    applyAtEnd: { pointerEvents: "none" }
  }
}

const menuItemProps = {
  initialPose: "close",
  open: {
    opacity: 1,
    x: 0,
    transition: { duration: 300 }
  },
  close: {
    opacity: 0,
    x: 30,
    transition: { duration: 200 }
  }
}

let menuStore = []
export const desktopMenuInit = () => {
  const menuItemElements = qq(".headerMenuItem")
  menuItemElements.map((el, index) => {
    if (el.tagName === "DIV") {
      const menuLevelEl = q(".headerMenuLevel", el.parentNode)

      menuStore[index] = {
        posed: pose(menuLevelEl, menuProps),
        children: []
      }

      menuStore[index].posed.addChild(
        q(".headerMenuLevelHeader", menuLevelEl),
        menuItemProps
      )

      addChildPosed(
        menuStore[index].posed,
        qq(".headerMenuLevelItem", menuLevelEl)
      )

      addNextChildrenPosed(
        qq(".headerMenuLevelItem", menuLevelEl),
        menuStore,
        index
      )

      menuStore[index].posed.addChild(
        q(".headerMenuLevelClose", menuLevelEl),
        menuItemProps
      )

      el.addEventListener("click", () => {
        closeLevelMenus(menuItemElements, menuStore)
        el.classList.add("headerMenuActive")
        menuStore[index].posed.set("open")
      })

      q(".headerMenuLevelClose", menuLevelEl).addEventListener("click", () => {
        menuStore[index].posed.set("close")
      })
    }
  })
}

const addChildPosed = (posed, elements) => {
  elements.map(nextEl => {
    posed.addChild(nextEl, menuItemProps)
  })
}

const closeLevelMenus = (elements, store) => {
  elements.map((els, idx) => {
    els.classList.remove("headerMenuActive")
    store[idx] && store[idx].posed.set("close")
  })
}

const addNextChildrenPosed = (elements, store, idx) => {
  elements.map((el, index) => {
    if (el.tagName === "DIV") {
      const menuLevelEl = q(".headerMenuLevelNext", el)
      store[idx].children[index] = { posed: pose(menuLevelEl, menuProps) }
      store[idx].children[index].posed.addChild(
        q(".headerMenuLevelNextHeader", menuLevelEl),
        menuItemProps
      )
      addChildPosed(
        store[idx].children[index].posed,
        qq(".headerMenuLevelNextItem", menuLevelEl)
      )
      store[idx].children[index].posed.addChild(
        q(".headerMenuLevelNextClose", menuLevelEl),
        menuItemProps
      )
      el.addEventListener("click", () => {
        el.classList.add("headerMenuLevelItemActive")
        closeNextLevelMenus(elements, store, idx)
        store[idx].children[index].posed.set("open")
      })

      q(".headerMenuLevelNextClose", menuLevelEl).addEventListener(
        "click",
        () => {
          setTimeout(() => {
            el.classList.remove("headerMenuLevelItemActive")
            store[idx].children[index].posed.set("close")
          }, 300)
        }
      )
    }
  })
}

const closeNextLevelMenus = (elements, store, idx) => {
  elements.map((els, index) => {
    els.classList.remove("headerMenuLevelItemActive")
    store[idx].children[index] && store[idx].children[index].posed.set("close")
  })
}
